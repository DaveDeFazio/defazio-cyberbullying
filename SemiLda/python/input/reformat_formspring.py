import pandas as pd

df = pd.read_csv("/home/mllab/Desktop/cyber_bullying/Data/Formspring/formspring.csv")

questions = df['Question']
#answers = df['Answer']
severity = df['Severity']

new_df = pd.DataFrame({'Severity': severity, 'Question': questions})
new_df = new_df[['Severity','Question']] #ensure correct order
new_df = new_df.dropna() #drop rows where severity isn't specified
new_df = new_df[new_df['Severity'] != 'n/a']
#new_df['Severity'] = pd.to_numeric(new_df['Severity'],  downcast='integer') #convert severity to int
new_df['Question'] = new_df['Question'].str[2:] #Remove Q: from each question

#change severities into categories
new_df['Severity'] = new_df['Severity'].astype(str) 

new_df['Severity'] = new_df['Severity'].replace(['0','1','2','3','4','5','6','7','8','9','10'],
	['NoBullying','Low','Low','Low','Med','Med','Med','High','High','High','High'])

#new_df['Severity'].replace(str(0.0),'NoBullying')
#new_df['Severity'] = new_df['Severity'].replace(["0", "1"], ["NoBullying", "Low"], inplace=True)

new_df.to_csv("real_train.txt", index=False, header=False)