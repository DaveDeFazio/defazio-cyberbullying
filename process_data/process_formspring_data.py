import os
from xml.etree import ElementTree
import pandas as pd

file_name = "XMLMergedFile.xml"
full_file = os.path.abspath(os.path.join("dataset", file_name))

dom = ElementTree.parse(full_file)

formspringid = dom.findall("FORMSPRINGID")
allposts = dom.findall("FORMSPRINGID/POSTS/TEXT")
root = dom.getroot()

userid = []
question = []
answer = []
severity = []
words = []

for child in root:
    tempid = child.find("USERID").text
    for element in child:
        if element.tag == "POST":
            temptext = element.find("TEXT").text
            tempq = temptext.split("<br>A:")[0]
            tempa = temptext.split("<br>A:")[1]
            tempsev = element.find("LABELDATA/SEVERITY").text
            tempwords = element.find("LABELDATA/CYBERBULLYWORD").text
            userid.append(tempid)
            question.append(tempq)
            answer.append(tempa)
            severity.append(tempsev)
            words.append(tempwords)


raw_data = {"UserID" : userid, "Question" : question, "Answer" : answer, "Bullying Words" : words, "Severity" : severity}


df = pd.DataFrame(raw_data, columns = ["UserID", "Question", "Answer", "Bullying Words", "Severity"])
df.to_csv("dataset/formspring_merged.csv")
